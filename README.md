# Repo Status

This repo is to hold information about private repos including build status, code coverage, etc., so custom dynamic shields.io badges can be generated for private repositories.

Data will be contained and should be updated in the badge-info.json
